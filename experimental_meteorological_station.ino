/*
 * pin number to BMP180 sensor
 * 05 - SCL
 * 04 - SDA
 * pin number  to DHT11 sensor
 * 10 - digital
 * pin number to GUVA-S12SD sensor
 * A0 - analogic
 * pin to reset
 * 9 - RST
 */
#include <Adafruit_BMP085.h>
#include <ESP8266WiFi.h>
#include "ThingSpeak.h"
#include <Wire.h>
#include <DHT.h>

#define seaLevelPressure_hPa 1013.25
#define DHTTYPE DHT11
#define DHTPIN 10

DHT dht (DHTPIN, DHTTYPE);
Adafruit_BMP085 bmp;
WiFiClient  client;

const char* pass  = "YOUR PASSWORD";
const char* ssid = "YOUR SSID";

float temperature = 0.00;
float humidity = 0.00;
float altitude = 0.00;
int pressure = 0;

const int analogPin = A0;
int sensorValue = 0;
int indexUV = 0;
float Vin = 3.300;
float voltage = 0.000;

//millis function to interval of 3 min
const long interval01 = ((1000 * 60) * 3);
unsigned long previousMillis01 = 0;

//millis function to interval of 1 min
const long interval02 = ((1000 * 60) * 1);
unsigned long previousMillis02 = 0;

//token e canal do ThingSpeak
const char * myWriteAPIKey = "XXXXXXXXXXXXXXXX";
unsigned long myChannelNumber = 1;

//reset in 45 days
unsigned long limitTime = ((((1000 * 60) * 60 ) * 24) * 45);
unsigned long currentMillis = 0;
const int pinReset = 9;

void setup()
{
  pinMode(analogPin, INPUT);
  digitalWrite(pinReset, HIGH);
  pinMode(pinReset, OUTPUT);

  WiFi.begin(ssid, pass);

  dht.begin();
  bmp.begin(); 
  ThingSpeak.begin(client);  
}

void loop()
{
  unsigned long currentMillis01 = millis();
  if (currentMillis01 - previousMillis01 >= interval01)
  {
    if((WiFi.status() != WL_CONNECTED))
    {
      WiFi.begin(ssid, pass);
    }
    previousMillis01 = currentMillis01;
  }
  
  temperature = dht. readTemperature ();
  humidity = dht. readHumidity ();
  pressure = bmp.readPressure();
  altitude = bmp.readAltitude();
  
  sensorValue = analogRead(analogPin);
  voltage = ((Vin / 1024) * sensorValue);
 
  if (voltage >= 0.000 && voltage < 0.050)
  {
    indexUV = 0;
  }
  if (voltage >= 0.050 && voltage < 0.227)
  {
    indexUV = 1;
  }
  if (voltage >= 0.227 && voltage < 0.318)
  {
    indexUV = 2;
  }
  if (voltage >= 0.318 && voltage < 0.408)
  {
    indexUV = 3;
  }
  if (voltage >= 0.408 && voltage < 0.503)
  {
    indexUV = 4;
  }
  if (voltage >= 0.503 && voltage < 0.606)
  {
    indexUV = 5;
  }
  if (voltage >= 0.606 && voltage < 0.696)
  {
    indexUV = 6;
  }
  if (voltage >= 0.696 && voltage < 0.795)
  {
    indexUV = 7;
  }
  if (voltage >= 0.795 && voltage < 0.881)
  {
    indexUV = 8;
  }
  if (voltage >= 0.881 && voltage < 0.976)
  {
    indexUV = 9;
  }
  if (voltage >= 0.976 && voltage < 1.079)
  {
    indexUV = 10;
  }
  if (voltage >= 1.079)
  {
    indexUV = 11;
  }
  
  //post values each 1 min
  unsigned long currentMillis02 = millis();
  if (currentMillis02 - previousMillis02 >= interval02)
  {
    ThingSpeak.setField(1, temperature);
    ThingSpeak.setField(2, humidity);
    ThingSpeak.setField(3, indexUV);
    ThingSpeak.setField(4, pressure);
 
    int x = ThingSpeak.writeFields(myChannelNumber, myWriteAPIKey);

    previousMillis02 = currentMillis02;
  }

}
