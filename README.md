<h1>Experimental Meteorological Station</h1>
<p>
This project aims to collect data through humidity, temperature, pressure and UV level sensors and send them to the ThingSpeak platform where they are available for consultation or display on a web page, for example.<br>
The sensors used were: BMP180, DHT11 and GUVA-S12SD.<br>
A small piece of code was also implemented that resets the uC every 45 days.
